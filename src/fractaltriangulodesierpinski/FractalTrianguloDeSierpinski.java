/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractaltriangulodesierpinski;

import javax.swing.JFrame;
import modelos.TrianguloSierpinski;

/**
 *
 * @author vicky
 */
public class FractalTrianguloDeSierpinski {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TrianguloSierpinski triangulo = new TrianguloSierpinski();// modelo
        PanelTrianguloSierpinski panel = new PanelTrianguloSierpinski(triangulo);// vista el panel conoce al modelo o sea al de arriba
        OyenteTrianguloSierpinski oyente =new OyenteTrianguloSierpinski(triangulo,panel ); // controlador 
        panel.addEventos(oyente);
        
        JFrame f= new JFrame(" triangulo sierpinski");
        f.setLocation(100,100);
        f.setSize(800,600);
        f.add(panel);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        
    }
    
}
